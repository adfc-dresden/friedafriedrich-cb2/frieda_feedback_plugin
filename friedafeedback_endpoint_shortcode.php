<?php

use CommonsBooking\Settings\Settings;

add_shortcode("frieda_feedback", "frieda_feedback_shortcode");

function frieda_feedback_shortcode($atts)
{
    // inject CSS to hide header (logo, menu, slogan) of webpage when rendering feedback endpoint page with 
    $content = <<<EOF
    <style>
    header {
        display: none;
    }
    main {
        padding: 1em;
    }
    </style>
    EOF;

    if (!isset($_GET["hash"]) or !isset($_GET["feedback"])) {
        $content .= "Daten für Feedback fehlen";
        return $content;
    }

    $inputhash = preg_replace("#\W#", "", $_GET["hash"]); // filter away everything but alphanumeric characters from hash-variable
    $inputfeedback = preg_replace("#\W#", "", $_GET["feedback"]);
    $inputcomment = isset($_GET['comment']) ? htmlspecialchars($_GET['comment']) : null;

    if (!in_array($inputfeedback, ['ok', 'attention', 'nouse'])) {
        $content .= "Feedback-Kodierung ungültig";
        return $content;
    }

    $args = [
        "name" => $inputhash,
        "post_type" => "cb_booking",
        "posts_per_page" => 1,
        "post_status" => "confirmed",
    ];

    $cb_booking_posts = get_posts($args);

    if (count($cb_booking_posts) != 1) {
        $content .= "Matching cb_booking not found";
        return $content;
    }

    $cb_booking_post = $cb_booking_posts[0];

    $post_id = $cb_booking_post->ID;

    $item_id = get_post_meta($post_id, "item-id", true);
    $location_id = get_post_meta($post_id, "location-id", true);

    if (!get_post_meta($post_id, "full-day", false)) {
        $content .= "Only implemented for full-day bookings yet";
        return $content;
    }

    // timezone: when a user books a bike from 10:00 to 12:00 local time,
    // it is stored in repetition-start and repetition-end timestampes,
    // as if he booked from 10:00 to 12:00 UTC.
    $utc_timezone = new DateTimeZone("UTC");
    $starttime = get_post_meta($post_id, "repetition-start", true);
    $endtime = get_post_meta($post_id, "repetition-end", true);

    $str_booking_interval = wp_date('d. F', $starttime, $utc_timezone) . " - " . wp_date('d. F', $endtime, $utc_timezone);

    $item_name = get_the_title($item_id);

    $user_id = get_post_field("post_author", $cb_booking_post);

    $user = get_user_by("id", $user_id);
    $user_login = $user->user_login;

    $user_email = $user->user_email;

    /* Check if text feedback was already given */
    if (strlen(get_post_meta($post_id, "feedback_text", true)) > 2) {
        $textcomment_already_in_db = true;
    } else {
        $textcomment_already_in_db = false;
    }

    if (!$textcomment_already_in_db) {
        update_post_meta($post_id, 'feedback_type', $inputfeedback);
        update_post_meta($post_id, 'feedback_text', $inputcomment);
        if (strlen($inputcomment) > 2) {
            send_feedback_to_admin(
                $user_email,
                $user_login,
                $item_id,
                $location_id,
                $str_booking_interval,
                $inputfeedback,
                $inputcomment
            );
        }
    }

    $email = Settings::getOption( 'commonsbooking_options_templates', 'emailheaders_from-email' );

    if (!isset($inputcomment)) {
        // page is being loaded with URL from e-mail, not as a result of 'submit'-action        
        if (!$textcomment_already_in_db) {
            // when no text comment is stored yet, it is still possible to chose another feedback type or add a text
            $content .= "Rückmeldung zu deiner Buchung vom Lastenrad <i>$item_name</i> (";
            $content .= $str_booking_interval;
            $content .= ")";

            if ($inputfeedback === "ok") {
                $content .= "<h2>Deine Rückmeldung: Alles in Ordnung</h2>";
            } elseif ($inputfeedback === "nouse") {
                $content .= "<h2>Deine Rückmeldung: Verleih fand nicht statt</h2>";
            } else {
                // 'attention'
                $content .= "<h2>Deine Rückmeldung: Es gab Probleme</h2>";
            }

            $content .= 'Ist dir etwas betreffend dieser Buchung aufgefallen? Probleme bei Verleih/Rückgabe? ';
            $content .= 'Technische Mängel oder Verschleiß am Rad? Eine kurze und möglichst genaue Beschreibung ist uns enorm wichtig, ';
            $content .= 'damit wir  uns schnell kümmern können.';

            $content .= '<form method="get">';
            $content .= '<p><textarea name="comment" rows="4" cols="50" style="width: 100%; max-width: 55em;">';
            $content .= $inputcomment;
            $content .= "</textarea></p>";
            $content .= '<input type="hidden" name="hash" value="' . $inputhash . '">';
            $content .= '<input type="hidden" name="feedback" value="' . $inputfeedback . '">';
            $content .= '<input type="submit" name="submit" value="Absenden" class="button">';
            $content .= '';
            $content .= '</form>';
        } else {
            // when text comment is already stored, do not allow any override of text or feedback type anymore
            $content .= '<h2>Eine Rückmeldung zu dieser Buchung ist schon vorhanden!</h2>';
            $content .= '<p>...aber du kannst sehr gern eine E-Mail an uns schreiben!</p>';
            $content .= '<p><a href="/">Zurück zur Buchungsplattform</a></p>';
            $content .= "<p>Uns per E-Mail kontaktieren: <a href='mailto://$email'>$email</a></p>";
        }
    } else {
        // 'comment' GET variable is defined and not null - might be of zero length or more, but is defined:
        // page is being loaded as a result of 'submit'-action 
        $content .= "<h2>Vielen Dank für die Rückmeldung!</h2>";
        $content .= '<p><a href="/">Zurück zur Buchungsplattform</a></p>';
        $content .= "<p>Uns per E-Mail kontaktieren: <a href='mailto://$email'>$email</a></p>";
    }

    return $content;
}



function send_feedback_to_admin($user_email, $user_login, $item_id, $location_id, $str_booking_interval, $inputfeedback, $inputcomment) {

    $sender_email = Settings::getOption( 'commonsbooking_options_templates', 'emailheaders_from-email' );
    $sender_email_name = Settings::getOption( 'commonsbooking_options_templates', 'emailheaders_from-name', 'sanitize_text_field' );

    $mailHeaders = [];

    $mailHeaders[] = "From: $sender_email_name <$sender_email>";
    $mailHeaders[] = "Reply-To: $user_email";
    $mailHeaders[] = "Content-Type: text/html";

    $recipient_email[] = $sender_email;
    
    $item_name_html = get_the_title($item_id);
    $item_name = html_entity_decode($item_name_html, ENT_QUOTES, 'UTF-8');

    $subject = "Nutzer-Rückmeldung zu $item_name";

    $message = "<!DOCTYPE html>";
    $message .= '<html lang="de">';
    $message .= "<head><title>$subject</title></head>";
    $message .= "<body>";
    $message .= "<p>Nutzer-Rückmeldung zu $item_name_html</p>";
    $message .= "<p>Nutzer: $user_login ($user_email)<br>";
    $message .= "Lastenrad: $item_name<br>";
    $message .= "Buchungszeitraum: $str_booking_interval<br>";
    $message .= "Feedback-Kategorie: <b>$inputfeedback</b><br>";
    $message .= "Freitexteingabe:</p>";
    $message .= "<p>'<i>$inputcomment</i>'</p>";

    $message .= "<p>Grüße, Tool für Lastenrad-Feedback</p>";
    $message .= "</body></html>";

    $cb_item_owner = get_field('cb_item_owner', $item_id);

    if ( $cb_item_owner && $cb_item_owner->ID > 0)
    {
        $owner_email = get_field('e-mail', $cb_item_owner->ID);
        $recipient_email[] = $owner_email;
    }

    if (function_exists('patenliste_getLocationPartners')) { // patenliste_getLocationPartners is part of buchungsinfo2

        $locationUsers = patenliste_getLocationPartners();

        $partners = isset($locationUsers[$location_id]) ? $locationUsers[$location_id] : [];

        $partnerEmailsArray = [];

        foreach ($partners as $partner) {
            $user = get_user_by('id', $partner);
            if (!$user || !$user->user_email) continue; // Skip if no user or no email
            if (in_array($user->user_email, $recipient_email, true)) continue;
            $partnerEmailsArray[] = $user->user_email;
        }

        if (!empty($partnerEmailsArray)) {
            $partnerEmailsString = implode(',', $partnerEmailsArray);
            $mailHeaders[] = "Cc: $partnerEmailsString";
        }
    }

    wp_mail($recipient_email, $subject, $message, $mailHeaders);
}


?>