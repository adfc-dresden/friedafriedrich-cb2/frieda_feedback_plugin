This Wordpress plugin provides a post-booking feedback system for a site based on Commonsbooking2.


## Feedback request e-mail
The Commonsbooking2 post-booking notification e-mail is used. Using a custom template (configured in CB2 preferences), the user is asked for feedback at the end of a booking. This plugin provides two additional template tags

 - DONATION_REQUEST(x) Generates a donation request text for cargo bike with item id x. Use together with CB2 template tag {{item:ID}}, like this: DONATION_REQUEST({{item:ID}})
 - FEEDBACKENDPOINT Expands to full URL of feedback endpoint, e.g. https://friedafriedrich.de/feedback

## Feedback endpoint / shortcode [frieda_feedback]
The plugin creates a page "feedback" on initial activation and adds nothing more than the shortcode [frieda_feedback] to the page. This page will be the target for the links from the post-booking notification e-mails. The user will be asked to provide additional details and a request for donations is shown on this page. 

## Donation request
The donation request is individual per Anbieter. Per Anbieter or Anbieterin, create a cb_item_owner post (see below) and add a donation request text (for example with bank account number or Paypal link).

## Sample template
Sample template for the CB2 post-booking notification e-mail. Note the use of CB2s built-in templates {{item:post_title}}, {{item:ID}} {{booking#post_name}} (CB2 treats delimiter # exactly like :, but : cannot be used in URLs).

    <p>Liebe Nutzerin, lieber Nutzer des Lastenrades <i>{{item:post_title}}</i>:</p>
          <p>Der ADFC Dresden und seine Partner bemühen sich, den Lastenrad-Verleih reibungslos zu gestalten. Daher sind wir auf präzise Rückmeldungen angewiesen. Bitte teile uns Mängel oder Probleme mit.</p>
          <p><i>Auch wenn du schon in der Verleihstation oder auf dem Ausleihzettel Mängel oder Probleme angegeben hast, erreichen diese die Lastenrad-Anbieter meist nicht schnell genug. Daher ist die Rückmeldung auf diesem Wege sehr wichtig.</i></p>
          <p>Bitte auf den zutreffenden Link klicken:</p>
          <table width="100%">
             <tr>
                <td>
                   <table style="border-spacing: 10px">
                      <tr>
                             <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=ok" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Alles in Ordnung!</a></td>
                             <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=nouse" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Verleih fand nicht statt</a></td>
                             <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=attention" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Es gab Probleme</a></td>
                      </tr>
                   </table>
                </td>
             </tr>
          </table>
    
    <p>DONATION_REQUEST({{item:ID}})</p>
    <p>Danke und viele Grüße,<br>
    freies Lastenrad Zittau - ein Projekt des ADFC Zittau</p>
    <p><small>Der ADFC Zittau ist eine Ortsgruppe des ADFC Sachsen e.V., Bautzner Straße 25, 01099 Dresden - <a href="https://www.adfc-sachsen.de/impressum">IMPRESSUM</a></small></p>

## Statistics
Create a private page with shortcode [frieda_feedback_statistics] to see user feedback.

## Create Custom Post Type 'Item Owner' and 'Iten Owner Data' fields

In ACF:

Add Custom Post Type:
Plural Name: Anbieter / "Item Owners"
Singular Name: Anbieter / "Item Owner"
Inhaltstyp-Schlüssel: cb_item_owner
Public / Öffentlich: yes
Hierarchisch: no
Erweiterte Einstellungen -> Allgemein -> "Editor" und "Beitragsbild" ausblenden
Erweiterte Einstellungen -> Sichtbarkeit -> Admin Parent Menu: "cb-dashboard"



Add Fieldgroup "Anbieter" / "Item Owner"
- In that field group, add Field "Anbieter" / "Item Owner":
  Field Type: "Beitrags-Objekt" / "Post object"
  Field Label: "Anbieter" / "Item Owner"
  Field Name: cb_item_owner
  Filter by Post Type: "Anbieter" / "Item Owner"
  Return: Post Object
  Select Multiple: no
  Validation: Required no. Allow Null yes.
- Location Rules of Fieldgroup:
  Show this filed if "Post Type" "is equal to" "Item"


Add Fieldgroup "Anbieterdaten" / "Item Owner Data"
- In that field group, add Field
  Field Type: "True / False"
  Field Label: "weiblich" / "Female Genus"
  Field Name: female_genus
- In that field group, add Field 
  Field Type: "Text Area"
  Field Label: "Anbieter-Spendenaufruf HTML":
  Field Name: donation_info_html  
- In that field group, add Field 
  Field Type: "E-Mail"
  Field Label: "E-Mail":
  Field Name: e-mail
- In that field group, add Field 
  Field Type: "Text Area"
  Field Label: "Anbieterverzeichnistext":
  Field Name: anbieterverzeichnistext  
- Location Rules of Fieldgroup:
  Show this filed if "Post Type" "is equal to" "Item Owner"