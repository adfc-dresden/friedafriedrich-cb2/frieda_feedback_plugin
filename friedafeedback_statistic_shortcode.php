<?php


add_shortcode("frieda_feedback_statistics", "frieda_feedback_statistics_shortcode");

function frieda_feedback_statistics_shortcode($atts)
{
    // inject CSS to hide header (logo, menu, slogan) of webpage when rendering feedback endpoint page with 
    $content = <<<EOF
    <style>
    header {
        display: none;
    }
    main {
        padding: 1em;
    }
    </style>
    EOF;


     $args = array(
        'post_type' => 'cb_booking',
        'posts_per_page' => -1,
        'orderby' => array( 'ID' => 'ASC' ),
        'post_status' => 'confirmed'
    );
    $query = new WP_Query($args);
    $content = '';
    if ($query->have_posts()) {
        $content .= "<pre style=\"font-family: 'Courier New', Courier, monospace;\">";
        $content .= "ID    ; Rückgabe         ; Rad     ; Station ; F-Code ; F-Text\r";
        while ($query->have_posts()) {
            $query->the_post();
            $repetition_start_timestamp = get_post_meta(get_the_ID(), 'repetition-start', true);
            $repetition_start_date = new DateTime("@$repetition_start_timestamp");
            $repetition_start_date->setTimezone(new DateTimeZone('UTC'));
            $repetition_start_formatted = $repetition_start_date->format('Y-m-d');
            
            $repetition_end_timestamp = get_post_meta(get_the_ID(), 'repetition-end', true);
            $repetition_end_date = new DateTime("@$repetition_end_timestamp");
            $repetition_end_date->setTimezone(new DateTimeZone('UTC'));
            $repetition_end_formatted = $repetition_end_date->format('Y-m-d H:i');
            
            $location_id = get_post_meta(get_the_ID(), 'location-id', true);
            $item_id = get_post_meta(get_the_ID(), 'item-id', true);

            $feedback_type = get_post_meta(get_the_ID(), 'feedback_type', true);
            $feedback_text = get_post_meta(get_the_ID(), 'feedback_text', true);
            
            $location_name = get_the_title($location_id);
            $item_name = get_the_title($item_id);
            $item_name = str_replace('ADFC ', '', $item_name);

            $fw_item_name = str_pad(mb_strimwidth($item_name, 0, 7),7);
            $fw_loc_name = str_pad(mb_strimwidth($location_name, 0, 7),7);
            $fw_feedback_type = str_pad(mb_strimwidth($feedback_type, 0, 6),6);

            $comment_wo_linebreaks = str_replace("\n"," ",$feedback_text);
            $comment_wo_linebreaks = str_replace("\r"," ",$comment_wo_linebreaks);

            $fw_id = sprintf('%-5d', get_the_ID());

            $content .= $fw_id . ' ; ';
            $content .= $repetition_end_formatted . ' ; ';
            $content .= $fw_item_name . ' ; ';
            $content .= $fw_loc_name . ' ; ';
            $content .= $fw_feedback_type . ' ; ';
            $content .= $comment_wo_linebreaks . "\r";
        }
        $content .= '</pre>';
    } else {
        $content .= 'No bookings found';
    }
    wp_reset_postdata();
    return $content;


}




?>