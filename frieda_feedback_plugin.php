<?php
/**
 * Plugin Name: Frieda & Friedrich Feedback Plugin for Commonsbooking2
 * Description: Provides a CB2 template for post-booking notification e-mail to request feedback and donantions
 * Author: Nils Larsen
 * Author URI: https://www.friedafriedrich.de
 */

require_once "friedafeedback_endpoint_shortcode.php";
require_once "friedafeedback_statistic_shortcode.php";
require_once "friedafeedback_template_tags.php";


// We use category descriptions (=term_description) for custom donation request HTML,
// but Wordpress kses will filter <p> and <br> tags out by default, so
// allow those tags explicitly in term descriptions
function customize_allowed_html($tags, $context)
{
    if (in_array($context, ["pre_term_description", "term_description"])) {
        $tags["p"] = [];
        $tags["br"] = [];
    }

    return $tags;
}
add_filter("wp_kses_allowed_html", "customize_allowed_html", 10, 2);

// the "bullet proof" buttons in the Frieda&Friedrich feedback e-mail
// requires css style display:block-inline, but wp_kses filters display
// out in e-mail templates as "unsafe". Add to "safe" list.
add_filter("safe_style_css", function($styles) {
    $styles[] = "display";
    return $styles;
});

function friedafeedback_activation_hook() {

    // Require active "commonsbooking" to allow activation
    if (!is_plugin_active('commonsbooking/commonsbooking.php')) {
        deactivate_plugins(basename(__FILE__));

        wp_die('Sorry, but Frieda Feedback Plugin requires commonsbooking to be installed and active.');
    }

    // Check if page 'feedback' (as endpoint for feedback request mails links exists or create it
    if (!friedafeedback_create_feedbackendpoint()) {
        wp_die('Sorry, could not create "feedback" page. Maybe it already exists but lacks the shortcode frieda_feedback');
    }

}
register_activation_hook(__FILE__, 'friedafeedback_activation_hook');

function friedafeedback_create_feedbackendpoint() : bool {
    $feedback_page = get_page_by_path('feedback');

    if ($feedback_page === null) {
        return wp_insert_post(array(
            'post_title'    => 'Feedback',
            'post_name'     => 'feedback',
            'post_content'   => '[frieda_feedback]',
            'post_status'   => 'publish',
            'post_type'     => 'page',
        ));
    } else {
        // Page exists, check if it contains the shortcode
        $page_content = $feedback_page->post_content;
        return has_shortcode($feedback_page->post_content, 'frieda_feedback');
    }
}