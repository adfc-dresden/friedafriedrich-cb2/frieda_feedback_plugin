<?php

// use CB2 hook filter 'commonsbooking_template_tag' to substitute DONATION_REQUEST(n)
// in e-mail-templates where n is the itemid of the cargo bike
function commonsbooking_template_tag_callback($string)
{
    $re = '/DONATION_REQUEST\((\d+)\)/';
    if (preg_match($re, $string, $matches) == 1) {
        $item_id = $matches[1];
        if (is_numeric($item_id) && (int) $item_id == $item_id) {
            $item_id = (int) $item_id;
            $substitution = generateHtmlSpendenaufruf($item_id);

            $string = preg_replace($re, $substitution, $string);
        }
    }

    $endpoint_page = get_page_by_path('feedback');

    if ($endpoint_page !== null) {
        $re = '/FEEDBACKENDPOINT/';
        $substitution = get_permalink($endpoint_page);
        $string = preg_replace($re, $substitution, $string);
    }

    return $string;
}
add_filter( 'commonsbooking_template_tag', 'commonsbooking_template_tag_callback', 10, 3 );


function generateHtmlSpendenaufruf($item_id)
{

    if (get_post_type($item_id) !== "cb_item") return null;

    $cb_item_owner = get_field('cb_item_owner', $item_id);

    if ( !$cb_item_owner || $cb_item_owner->ID <= 0 ) return null;

    $donation_info_html = get_field('donation_info_html', $cb_item_owner->ID);

    if ( empty($donation_info_html) || strlen($donation_info_html) < 10 ) return null;

    $cb_item_owner_female_genus = get_field('female_genus', $cb_item_owner->ID);

    $anbieter_artikel_dativ = $cb_item_owner_female_genus ? "der" : "dem";
    $anbieter_bestimmt =  $cb_item_owner_female_genus ? "Anbieterin" : "Anbieter";
    $anbieter = get_the_title($cb_item_owner->ID);
    $itemname = get_the_title($item_id);

    // Sanitize the donation info HTML
    $allowed_tags = array(
        'p' => array(),
        'br' => array(),
        'b' => array(),
        'strong' => array(),
        'a' => array(
            'href' => array(),
            'title' => array(),
        ),
    );
    $donation_info_html_sanitized = wp_kses($donation_info_html, $allowed_tags);

    // Adjust the item name if it contains the owner's name
    if (strstr($itemname, $anbieter))
    {
        $itemname = str_replace("$anbieter ", "", $itemname);
    }

    $html = "<p>Bitte hilf $anbieter_artikel_dativ <strong>" . esc_html($anbieter) . "</strong> als $anbieter_bestimmt von <strong>" . esc_html($itemname) . "</strong> mit deiner Spende, um dieses Lastenrad gut in Schuss zu halten (Spendenempfehlung 5-10 Euro pro Ausleihtag). Wenn du schon gespendet hast: Danke!</p>";

    $html .= "<p>" . $donation_info_html_sanitized . "</p>";

    return $html;

}

?>